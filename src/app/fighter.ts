export interface Fighter {
    name: string,
    hp: number,
    acc: number,
    def: number,
    attDc: number,
    str: number,
    accDc: number,
    init: number
}
