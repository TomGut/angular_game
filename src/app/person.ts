import { Fighter } from "./fighter";

export abstract class Person implements Fighter {
    constructor(public name: string, hp: number, public acc: number, public def: number,
        public attDc: number, public str: number, public accDc: number, public init: number) {
            this._hpCurrent = this.maxHp = hp;
    }

    private _hpCurrent: number;
    public maxHp: number;
    //this provide value for checkup in loop at combat() - Dragon and Warrior inherits from Person
    public isAlive = true;

    public get hp(): number {
        return this._hpCurrent;
    }

    public set hp(_hp: number){
        this._hpCurrent = _hp;
        this.isAlive = this._hpCurrent > 0;
    }

    abstract hit(): void;
    abstract atk(target: Person): void;
    abstract specialAtk(target: Array<Person>);
}
