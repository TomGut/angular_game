import { Person } from "./person";
import { DiceService } from "./dice.service";
import { AppComponent } from "./app.component";

export class Warrior extends Person {
    constructor(name: string, hp: number, acc: number, def: number,
        attDc: number, str: number, accDc: number, init: number, public diceService: DiceService, public infos: Array<String>, public image: string) {
        super(name, hp, acc, def, attDc, str, accDc, init);
    }

    hit(): boolean {
        return this.acc > this.diceService.rollDice(100);
    };

    atk(target: Person): void {
        if (this.hit()) {
            let attackValue = (this.str + this.diceService.rollDice(this.attDc)) - target.def;
            target.hp -= attackValue;
            this.infos.push(`${this.name} attack ${target.name} for ${attackValue} hp`);
            console.log(`${this.name} attack ${target.name} for ${attackValue} hp`);
        } else {
            this.infos.push(`${this.name} miss`);
            console.log(`${this.name} miss`);
        }
    };

    specialAtk(target: Person[]) {
        throw new Error("Method not implemented.");
    }
}
