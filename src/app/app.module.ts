import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DiceService } from './dice.service';
import { HeroComponent } from './hero/hero.component';
import { DragonComponent } from './dragon/dragon.component';

@NgModule({
  declarations: [
    AppComponent,
    HeroComponent,
    DragonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  //serwisy zaciągamy w providers
  providers: [
    DiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
