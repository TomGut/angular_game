import { Component, OnInit, Input } from '@angular/core';
import { Dragon } from '../dragon';

@Component({
  selector: 'app-dragon',
  templateUrl: './dragon.component.html',
  styleUrls: ['./dragon.component.css']
})
export class DragonComponent implements OnInit {

  @Input()
  dragon: Dragon;

  constructor() { }

  ngOnInit() {
  }

}
