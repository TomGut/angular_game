import { Component, OnInit, Input } from '@angular/core';
import { Warrior } from '../warrior';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css']
})
export class HeroComponent implements OnInit {

  @Input()
  warrior: Warrior;

  constructor() { }

  ngOnInit() {
  }

}
