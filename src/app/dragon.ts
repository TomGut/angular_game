import { Person } from "./person";
import { DiceService } from "./dice.service";

export class Dragon extends Person {
    constructor(name: string, hp: number, acc: number, def: number,
        attDc: number, str: number, accDc: number, init: number, public diceService: DiceService, public info: Array<String>, public image: string) {
        super(name, hp, acc, def, attDc, str, accDc, init);
    }

    hit(): boolean {
        return this.acc > this.diceService.rollDice(100);
    };

    atk(target: Person): void {
        if (this.hit()) {
            let attackValue = (this.str + this.diceService.rollDice(this.attDc)) - target.def;
            target.hp -= attackValue;
            this.info.push(`${this.name} attack ${target.name} for ${attackValue} hp`);
            console.log(`${this.name} attack ${target.name} for ${attackValue} hp`);
        } else {
            this.info.push(`${this.name} miss`);
            console.log(`${this.name} miss`);
        }
    };

    specialAtk(target: Person[]) {
        for(let i=0; i<target.length; i++){
            this.atk(target[i]);
        }
    }
}
