import { Warrior } from "./warrior";
import { Person } from "./person";
import { DiceService } from "./dice.service";
import { AppComponent } from "./app.component";

export class Healer extends Warrior {
    constructor(name: string, hp: number, acc: number, def: number,
        attDc: number, str: number, accDc: number, init: number, diceService: DiceService, infos: Array<String>, image: string) {
        super(name, hp, acc, def, attDc, str, accDc, init, diceService, infos, image);
    }
    //custom function we provide array of persons
    heal(target: Person[]): any {
        for(let i=0; i<target.length; i++){
            target[i].hp += 30;
        }
    }
}
