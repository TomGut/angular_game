import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DiceService {

  constructor() { }

  rollDice(max: number): number {
    return Math.floor((Math.random() * max) + 1);
}

  initCalc(): number {
    return this.rollDice(10);
}

}
