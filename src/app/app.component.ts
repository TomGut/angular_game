import { Component, OnInit } from '@angular/core';
import { Warrior } from './warrior';
import { Healer } from './healer';
import { Dragon } from './dragon';
import { DiceService } from './dice.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  ngOnInit(): void {
    this.combat();
  }
  title = 'angularGame';
  infos = new Array<String>();
  //creation of array for team
  team: Array<Warrior>;
  dragon: Dragon;

  constructor(public diceService: DiceService){
  }

  combat(): any {
    
    //filling out team array
    this.team = [
      //(name, hp, acc, def, attDc, str, accDc, init, diceservice)
      new Warrior("War1", 50, 80, 10, 10, 100, 10, 10, this.diceService, this.infos, "../assets/war1.jpg"),
      new Warrior("War2", 90, 90, 10, 10, 100, 10, 10, this.diceService, this.infos, "../assets/war2.png"),
      new Warrior("War3", 70, 80, 10, 10, 100, 10, 10, this.diceService, this.infos, "../assets/war3.jpg"),
      new Warrior("War4", 80, 70, 10, 10, 100, 10, 10, this.diceService, this.infos, "../assets/woman.jpg"),
      new Healer("Heal1", 90, 70, 10, 10, 100, 10, 10, this.diceService, this.infos, "../assets/healer.jpg")
    ];

    this.dragon = new Dragon("Drag", 1000, 90, 10, 100, 30, 10, 10, this.diceService, this.infos, "../assets/dragon.jpg");

    while (true) {
      //counter to keep values from loop - checkup is death team members is equal for team.length 
      let counter = 0;
      //checks if any team member or dragon is alive - attack
      for (let i = 0; i < this.team.length; i++) {
        if (this.team[i].isAlive && this.dragon.isAlive) {
          //if team member is healer - heals
          if (this.team[i] instanceof Healer) {
            //projection of team[i] to Healer class
            (this.team[i] as Healer).heal(this.team);
            this.infos.push(this.team[i].name + " heals");
            console.log(this.team[i].name + " heals")
          }
          //team attack dragon
          this.team[i].atk(this.dragon);
          this.infos.push("dragon hp is: " + this.dragon.hp);
          console.log("dragon hp is: " + this.dragon.hp);
        }
        //counter to check is all team members are dead
        else {
          counter++;
          this.infos.push("Team member named " + this.team[i].name + " is dead");
          console.log("Team member named " + this.team[i].name + " is dead");
        }
      }
      //if dead break the loop
      if (counter == this.team.length) break;
      //if dargon is alive perform special attack
      if (this.dragon.isAlive) {
        this.dragon.specialAtk(this.team);
        //if dragon is dead break
      } else {
        this.infos.push("Dragon named " + this.dragon.name + " is dead");
        console.log("Dragon named " + this.dragon.name + " is dead");
        break;
      }
    }
  }
}